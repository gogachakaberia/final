package com.example.afinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class ChangePasswordActivity : AppCompatActivity() {

    private lateinit var editTextPasswordChange : EditText
    private lateinit var editTextPasswordChangeRepeat : EditText
    private lateinit var buttonChange : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        init()
    }


    private fun init() {
        editTextPasswordChange = findViewById(R.id.editTextPasswordChange)
        editTextPasswordChangeRepeat = findViewById(R.id.editTextPasswordChangeRepeat)
        buttonChange = findViewById(R.id.buttonChange)
    }

}
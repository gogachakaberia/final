package com.example.afinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ProfileActivity : AppCompatActivity() {

    private lateinit var buttonChangePassword : Button
    private lateinit var buttonLogOut : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
        changeListeners()

    }

    private fun init() {
        buttonChangePassword = findViewById(R.id.buttonChangePassword)
        buttonLogOut = findViewById(R.id.buttonLogOut)
    }

    private fun changeListeners() {
        buttonChangePassword.setOnClickListener {
            intent = Intent(this, ChangePasswordActivity::class.java )
            startActivity(intent)
        }
    }

}